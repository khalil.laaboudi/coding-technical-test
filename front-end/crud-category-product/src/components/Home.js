import React from 'react'
import CategoriesList from './CategoriesList'
import { Heading } from './Heading'
import { ProductList } from './ProductList'
export const Home = () => {
    return (
        <>
            <Heading />
            <CategoriesList/>
            {/* <ProductList /> */}
        </>
    )
}
