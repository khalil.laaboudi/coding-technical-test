import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ListGroup, ListGroupItem, Button } from "reactstrap";
import { GlobalContext } from "../context/GlobalState";

export const ProductList = () => {
  const { products } = useContext(GlobalContext);
  console.log(products);

  return (
    <ListGroup className="mt-4">
      {products.map((product) => (
        <ListGroupItem className="d-flex justify-content-between">
          <strong>{product.name}</strong>
          <div className="ml-auto">
            <Link className="btn btn-warning mr-1" to={`/edit/${product.id}`}>
              Edit
            </Link>
            <Button color="danger">Delete</Button>
          </div>
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};
