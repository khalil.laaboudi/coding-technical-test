import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home } from "./components/Home";
import { EditProduct } from "./components/EditProduct";
import { AddProduct } from "./components/AddProduct";
import Collapsible from 'react-collapsible';

import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <div style={{ maxWidth: "30rem", margin: "4rem auto" }}></div>
      {/* <GlobalProvider> */}
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/add" component={AddProduct} />
            <Route path="/edit/:id" component={EditProduct} />
            {/* <Home />
          <AddProduct />
          <EditProduct /> */}
          </Switch>
        </Router>
      {/* </GlobalProvider> */}
    </div>
  );
}

export default App;
