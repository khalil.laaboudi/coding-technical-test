package com.codingChallenge.kotlinspringbootmysql.controller

import com.codingChallenge.kotlinspringbootmysql.model.Category
import com.codingChallenge.kotlinspringbootmysql.repository.CategoryRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = arrayOf("http://localhost:3000"))
class CategoryController(private val categoryRepository: CategoryRepository) {

    @GetMapping("/categories")
    fun getAllCategorys(): List<Category> =
            categoryRepository.findByParentCategoryNull();


    @PostMapping("/categories")
    fun createNewCategory(@Valid @RequestBody category: Category): Category {
        return categoryRepository.save(category);
    }


    @PutMapping("/categories/{id}")
    fun updateCategoryById(@PathVariable(value = "id") categoryId: Long,
                          @Valid @RequestBody newCategory: Category): ResponseEntity<Category> {

        return categoryRepository.findById(categoryId).map { existingCategory ->
            val updatedCategory: Category = existingCategory
                    .copy(title = newCategory.title)
            ResponseEntity.ok().body(categoryRepository.save(updatedCategory))
        }.orElse(ResponseEntity.notFound().build())
    }



    @DeleteMapping("/categories/{id}")
    fun deleteCategoryById(@PathVariable(value = "id") categoryId: Long): ResponseEntity<Void> {
        return categoryRepository.findById(categoryId).map { category  ->
            categoryRepository.delete(category)
            ResponseEntity<Void>(HttpStatus.OK)
        }.orElse(ResponseEntity.notFound().build())

    }
}

