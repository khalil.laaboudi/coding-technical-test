package com.codingChallenge.kotlinspringbootmysql.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.lang.Nullable
import javax.persistence.*
import javax.validation.constraints.Null


@Entity
@Table(name = "category")
data class Category (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long = 0,

        @Column(name = "title")
        val title: String = "",

        @ManyToOne
        @JoinColumn(name = "parentCategoryId")
        @JsonIgnore
        var parentCategory: Category?,


        @OneToMany(mappedBy = "parentCategory")
        var categories : List<Category>? ,

        @OneToMany(mappedBy = "parentCategory")
        var products : List<Product>? ,


)

