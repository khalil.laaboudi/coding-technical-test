package com.codingChallenge.kotlinspringbootmysql.model

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

//import javax.validation.constraints.NotBlank

@Entity
@Table(name = "product")
data class Product(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long = 0,

//        @get: NotBlank
        @Column(name = "title")
        val title: String = "",

//        @get: NotBlank
        @Column(name = "price")
        val price: Double = 0.0,

        @ManyToOne
        @JoinColumn(name ="parentCategoryId")
        @JsonIgnore
        var parentCategory: Category
)
